--create database inspectoratPolitie
--use inspectoratPolitie

create table InspectoratPolitie(
NumeIP varchar(45) not null,
Nume varchar(45), 	
Prenume varchar(45),
AdresaInsp varchar(45),
NrTefelon varchar(20),

primary key(NumeIP)
);


create table SectiePolitie(
NumeSectie varchar(45) not null,
AdresaSectie varchar(45),
SpecificSectie varchar(45),
NumeIP varchar(45),

primary key(NumeSectie),
foreign key(NumeIP) references InspectoratPolitie(NumeIP)
);

create table AgentPolitie(
ID varchar(45),
Nume varchar(45),
Prenume varchar(45),
AdresaAgent varchar(45),
PregatireProfesionala varchar(45),
ConduitaMorala varchar(45),
NrCazuriRezolvate int,
Functie varchar(45),
NumeSectieAg varchar(45),

primary key(ID),
foreign key(NumeSectieAg) references SectiePolitie(NumeSectie)
);

create table Categorie(
NumeCategorie varchar(45) not null,

primary key(NumeCategorie)
);

create table Dosar(
NumeDosar varchar(45) not null,
Descriere varchar(45),
StatusDosar varchar(45),
DownloadDosar mediumblob,
NumeCateg varchar(45),

primary key(NumeDosar),
foreign key(NumeCateg) references Categorie(NumeCategorie)
);

create table AgentDosar(
ID_Agent varchar(45),
NumeDosarAg varchar(45),

foreign key(ID_Agent) references AgentPolitie(ID),
foreign key(NumeDosarAg) references Dosar(NumeDosar)
);

create table SectieCategorie(
NumeSectieC varchar(45),
NumeCategS varchar(45),

foreign key(NumeSectieC) references SectiePolitie(NumeSectie),
foreign key(NumeCategS) references Categorie(NumeCategorie)
);


insert into InspectoratPolitie values(
'Inspectorat Politie Dolj',
'Gradinaru',
'Andrei',
'Str. Vulturi nr. 19',
'0251407510'
);

insert into sectiepolitie values(
'Sectia I Politie',
'str. Libertăţii, Craiova',
'Serviciu rutier',
'Inspectorat Politie Dolj'
);

insert into sectiepolitie values(
'Sectia II Politie',
'str. Dr. Dimitrie Gerota, Craiova',
'Serviciu de investigare a fraudelor',
'Inspectorat Politie Dolj'
);


insert into sectiepolitie values(
'Sectia III Politie',
'str. Henri Coandă, Craiova',
'Serviciu persoane disparute',
'Inspectorat Politie Dolj'
);

insert into sectiepolitie values(
'Sectia IV Politie',
'str. Paltinis, Craiova',
'Serviciu persoane disparute',
'Inspectorat Politie Dolj'
);

insert into sectiepolitie values(
'Sectia V Politie',
'str. N. Coculescu, Craiova',
'Serviciu obiecte furate',
'Inspectorat Politie Dolj'
);

insert into sectiepolitie values(
'Sectia VI Politie',
'str. Liberare, Craiova',
'Serviciu obiecte furate',
'Inspectorat Politie Dolj'
);

insert into AgentPolitie values(
'87892',
'Stuparu',
'Constatin',
'str. Gasda',
'Academia de Politie „Alexandru Ioan Cuza"',
'disponibilitatea',
12,
'subcomisar',
'Sectia VI Politie'
);

insert into AgentPolitie values(
'134322',
'Cojocaru',
'Nicusor',
'str. Iones',
'Şcoala de Agenţi "Septimiu Mureşan"',
'respectul',
10,
'adjunct sef sectie',
'Sectia I Politie'
);

insert into AgentPolitie values(
'457211',
'Badea',
'Sorin',
'str. Eroilor',
'Scoala de Agenti "Vasile Lascar"',
'profesionalismul',
10,
'sef sectie',
'Sectia II Politie'
);

insert into AgentPolitie values(
'9124511',
'Grigore',
'Bogdan',
'str. Nosa',
'Şcoala de Agenţi "Septimiu Mureşan"',
'prioritatea interesului public',
15,
'adjunct sef sectie',
'Sectia III Politie'
);

insert into AgentPolitie values(
'123211',
'Proca',
'Cristina',
'str. Ena',
'Scoala de Agenti "Vasile Lascar"',
'transparenţa',
7,
'comisar sef',
'Sectia IV Politie'
);

insert into AgentPolitie values(
'5411223',
'Priboi',
'Ion',
'str. Iliescu',
'Academia de Politie „Alexandru Ioan Cuza"',
'loialitatea',
17,
'sef sectie',
'Sectia V Politie'
);

insert into Categorie values(
'Infractiuni in regim rutier'
);

insert into Categorie values(
'Infractiuni in domeniul silvic'
);

insert into Categorie values(
'Coruptie'
);

insert into Categorie values(
'Infractiuni economice'
);

insert into Categorie values(
'Infractiuni contra patrimoniului'
);

insert into Categorie values(
'Infractiuni contra umanitatii'
);

insert into Dosar(NumeDosar, Descriere, StatusDosar, NumeCateg) values(
'22X3',
'Autovehicul neinmatriculat',
'In desfasurare',
'Infractiuni in regim rutier'
);

insert into Dosar(NumeDosar, Descriere, StatusDosar, NumeCateg) values(
'21ff3',
'Fara permis conducere',
'In desfasurare',
'Infractiuni in regim rutier'
);

insert into Dosar(NumeDosar, Descriere, StatusDosar, NumeCateg) values(
'1qag54',
'Taiere arbori fara autorizatie',
'In desfasurare',
'Infractiuni in domeniul silvic'
);

insert into Dosar(NumeDosar, Descriere, StatusDosar, NumeCateg) values(
'75nh',
'Furt de arbori',
'In desfasurare',
'Infractiuni in domeniul silvic'
);

insert into Dosar(NumeDosar, Descriere, StatusDosar, NumeCateg) values(
'11XC4',
'Luare mita',
'Solutionat',
'Coruptie'
);

insert into Dosar(NumeDosar, Descriere, StatusDosar, NumeCateg) values(
'988sdg',
'Abuz in serviciu',
'Solutionat',
'Coruptie'
);

insert into Dosar(NumeDosar, Descriere, StatusDosar, NumeCateg) values(
'297az3',
'Evaziune fiscala',
'In desfasurare',
'Coruptie'
);

insert into AgentDosar values(
'87892',
'22X3'
);

insert into AgentDosar values(
'5411223',
'297az3'
);

insert into AgentDosar values(
'134322',
'21ff3'
);

insert into AgentDosar values(
'457211',
'1qag54'
);

insert into AgentDosar values(
'9124511',
 '11XC4'
);

insert into AgentDosar values(
'123211',
'75nh'
);

insert into SectieCategorie values(
'Sectia I Politie',
'Coruptie'
);

insert into SectieCategorie values(
'Sectia II Politie',
'Infractiuni in regim rutier'
);

insert into SectieCategorie values(
'Sectia III Politie',
'Infractiuni in domeniul silvic'
);

insert into SectieCategorie values(
'Sectia IV Politie',
'Infractiuni economice'
);

insert into SectieCategorie values(
'Sectia V Politie',
'Coruptie'
);

insert into SectieCategorie values(
'Sectia VI Politie',
'Infractiuni contra patrimoniului'
);

